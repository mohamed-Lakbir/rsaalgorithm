

import java.math.BigInteger;

public class RsaAlgorithm {


    public int calculatePhi(int a,int b){
        a=a-1;
        b=b-1;
        return a*b;
    }


    public BigInteger rsaAlgorithm ( BigInteger massage, BigInteger e, BigInteger n){

        massage= massage.modPow(e,n);

        return massage;
    }

    public BigInteger createPrivatKey(BigInteger e, BigInteger phi){

      e=e.modInverse(phi);

      return e;
    }




}
