

import java.math.BigInteger;
import java.util.Scanner;

public class RsaMain {


    public static void main (String []args){

        RsaAlgorithm algo = new RsaAlgorithm();

        System.out.println("phi:");

        System.out.println(algo.calculatePhi(7,77));

        Scanner sc = new Scanner(System.in);
        System.out.print("Welche Zahl soll verschlüsselt werden :");
        String input = sc.next();
        System.out.println("");
        System.out.println("Encrypt : ");

        input = algo.rsaAlgorithm(new BigInteger(input), new BigInteger("23"), new BigInteger("77")).toString();
        System.out.println(input);

        System.out.println("Decrypt : ");

        System.out.println(algo.rsaAlgorithm(new BigInteger(String.valueOf(input)), new BigInteger("47"), new BigInteger("77")));

        System.out.println("Rsa mit 2048 Bit modul");

        System.out.println(algo.rsaAlgorithm(new BigInteger("10"), new BigInteger("3"),new BigInteger("5").pow(2048)));





    }
}
